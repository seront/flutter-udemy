import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class PairOutput extends StatefulWidget {
  final String name;
  final String dropdownValue = 'One';

  // Fields in a Widget subclass are always marked "final".

  const PairOutput({
    @required this.name,
  }) : assert(name != null);

  @override
  _PairOutputState createState() => _PairOutputState();
}

class _PairOutputState extends State<PairOutput> {
  String dropdownValue = 'One';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
          child: Center(
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(40.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(color: Colors.deepPurple),
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: <String>['One', 'Two', 'Free', 'Four']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Center(
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
