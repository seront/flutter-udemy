import 'package:flutter/material.dart';

import 'package:hello_rectangle/pair_input.dart';
import 'package:hello_rectangle/pair_output.dart';
import 'unit.dart';

class ConverterRoute extends StatefulWidget {
  final String name;
  final ColorSwatch color;
  final List<Unit> units;

  const ConverterRoute({
    @required this.name,
    @required this.color,
    @required this.units,
  })
      : assert(name != null),
        assert(color != null),
        assert(units != null);

  @override
  _ConverterRouteState createState() => _ConverterRouteState(name: name, color: color, units: units);
}

class _ConverterRouteState extends State<ConverterRoute> {
  final String name;
  final ColorSwatch color;
  final List<Unit> units;

   _ConverterRouteState({
    @required this.name,
    @required this.color,
    @required this.units,
  })
      : assert(name != null),
        assert(color != null),
        assert(units != null);

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      elevation: 0.0,
      title: Text(
        name,
        style: TextStyle(
          color: Colors.black,
          fontSize: 30.0,
        ),
      ),
      centerTitle: true,
      backgroundColor: color,
    );

    return Scaffold(
        appBar: appBar,
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              PairInput(name: name),
              Center(
                child: Icon(
                  Icons.cake,
                  size: 60.0,
                ),
              ),
              PairOutput(name: name)
            ]));
  }
}
