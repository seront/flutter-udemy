import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class PairInput extends StatefulWidget {
  final String name;
  final String dropdownValue = 'One';

  // Fields in a Widget subclass are always marked "final".

  const PairInput({
    @required this.name,
  }) : assert(name != null);

  @override
  _PairInputState createState() => _PairInputState();
}

class _PairInputState extends State<PairInput> {
  String dropdownValue = 'One';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
//      key: _formKey,
          child: Center(
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(40.0),
            child: Column(
//        height: 50.0,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: DropdownButton<String>(
                    value: dropdownValue,
//            icon: Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(color: Colors.deepPurple),
//            underline: Container(
//              height: 2,
//              color: Colors.deepPurpleAccent,
//            ),
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: <String>['One', 'Two', 'Free', 'Four']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
                Center(
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      )),
    );
  }
}
